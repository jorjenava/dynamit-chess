/**
 * Dynamit Node Hackathon
 * Chess Game Classes
 *
 * @author Anthony Bennett <anthony@dynamit.us>
 * @since 2013/04/27
 */

(function(root, _) {
	// games in progress
	var games = [],
		ch = require('./chess.js'),
		chess = new ch.Chess();

	// Game class
	var Game = function(gameId, players, board) {
		this._id = gameId;
		this.players = (players || []);
		this.board = (board || (new Board()));
	};
	_.extend(Game.prototype, {
		addPlayer: function() {
			var player;

			// white player / player 1
			if (!this.players.length) {
				player = new Player();
			// black player / player 2
			} else if (1 == this.players.length) {
				player = new Player("BLACK", "Player 2", false);
			// viewers
			} else {
				player = new Player("VIEWER", ("Viewer " + (this.players.length - 1)), false);
			}

			this.players.push(player);

			return player;
		},
		removePlayer: function(player) {
			var index = _.indexOf(this.players, player),
				endGame = ((0 == index) || (1 == index));

			this.players = _.without(this.players, player);

			return endGame;
		}
	});

	// Board class
	var Board = function(pieces) {
		this.pieces = (pieces || _.map([
				{ xy: "a1", type: "ROOK", color: "WHITE" },
				{ xy: "b1", type: "KNIGHT", color: "WHITE" },
				{ xy: "c1", type: "BISHOP", color: "WHITE" },
				{ xy: "d1", type: "QUEEN", color: "WHITE" },
				{ xy: "e1", type: "KING", color: "WHITE" },
				{ xy: "f1", type: "BISHOP", color: "WHITE" },
				{ xy: "g1", type: "KNIGHT", color: "WHITE" },
				{ xy: "h1", type: "ROOK", color: "WHITE" },
				{ xy: "a2", type: "PAWN", color: "WHITE" },
				{ xy: "b2", type: "PAWN", color: "WHITE" },
				{ xy: "c2", type: "PAWN", color: "WHITE" },
				{ xy: "d2", type: "PAWN", color: "WHITE" },
				{ xy: "e2", type: "PAWN", color: "WHITE" },
				{ xy: "f2", type: "PAWN", color: "WHITE" },
				{ xy: "g2", type: "PAWN", color: "WHITE" },
				{ xy: "h2", type: "PAWN", color: "WHITE" },
				{ xy: "a8", type: "ROOK", color: "BLACK" },
				{ xy: "b8", type: "KNIGHT", color: "BLACK" },
				{ xy: "c8", type: "BISHOP", color: "BLACK" },
				{ xy: "d8", type: "QUEEN", color: "BLACK" },
				{ xy: "e8", type: "KING", color: "BLACK" },
				{ xy: "f8", type: "BISHOP", color: "BLACK" },
				{ xy: "g8", type: "KNIGHT", color: "BLACK" },
				{ xy: "h8", type: "ROOK", color: "BLACK" },
				{ xy: "a7", type: "PAWN", color: "BLACK" },
				{ xy: "b7", type: "PAWN", color: "BLACK" },
				{ xy: "c7", type: "PAWN", color: "BLACK" },
				{ xy: "d7", type: "PAWN", color: "BLACK" },
				{ xy: "e7", type: "PAWN", color: "BLACK" },
				{ xy: "f7", type: "PAWN", color: "BLACK" },
				{ xy: "g7", type: "PAWN", color: "BLACK" },
				{ xy: "h7", type: "PAWN", color: "BLACK" }
			], function(snapshot) {
				var xy = snapshot.xy.split(""),
					state = new State(xy[0], xy[1], true);

				return new Piece(snapshot.type, snapshot.color, state);
			}));
	};
	_.extend(Board.prototype, {
		changeState: function(move) {
			// Find out where piece is being moved from.
			var piece = _.find(this.pieces, function(piece) {
				return (piece.state.alive &&
						(piece.state.x + piece.state.y) == move.origin);
			});
			if (!piece) {
				return false;
			}

			// Call move in api.
			var result = chess.move({
				from: move.origin,
				to: move.destination
			});

			// if the move is valid
			if (result) {
				// kill characters if necesesary
				if(result.flags.indexOf('c') > -1) {
					var captured = _.find(this.pieces, function(piece) {
						return (piece.state.alive &&
								(piece.state.x + piece.state.y) == move.destination);
					});
					if (captured) {
						captured.state.alive = false;
					}
				}

				// update the piece's location
				piece.position = move.destination;
				piece.state.x = move.destination.split("")[0];
				piece.state.y = move.destination.split("")[1];
			}

			return true;
		},
		getSnapshot: function() {
			var snapshot = [];

			_.each(this.pieces, function (piece) {
				if (piece.state.alive) {
					snapshot.push(piece.getSnapshot());
				}
			});

			return snapshot;
		}
	});

	// Viewer class
	var Viewer = function(name) {
		this.name = (name || _.uniqueId("Viewer "));
	};

	// Player class
	var Player = function(color, name, active) {
		this.color = (color || "WHITE");
		this.name = (name || "Player 1");
		this.active = true;
	};

	// Piece class
	var Piece = function(type, color, state) {
		this.type = type;
		this.color = (color || "WHITE");
		this.state = (state || []);
		this.position = state.x + state.y;
	};
	_.extend(Piece.prototype, {
		getSnapshot: function() {
			return {
				xy: (this.state.x + this.state.y),
				type: this.type,
				color: this.color
			};
		}
	});

	// State class
	var State = function(x, y, alive) {
		this.x = (x || "a");
		this.y = (y || 1);
		this.alive = (("undefined" == typeof alive) ? alive : true);
	};

	// finally, export our API
	exports.listGames = function() {
		return _.pluck(games, "_id");
	};
	exports.join = function(gameId) {
		var game,
			player;

		// find given game by id
		if (gameId) {
			game = _.findWhere(games, { _id: gameId });
			if (!game) {
				// create a new game
				game = new Game(gameId);
				games.push(game);
			}
		}

		// add player / viewer to game
		player = game.addPlayer();

		// return the game and player
		return { game: game, player: player };
	};
}(this, require("underscore")));