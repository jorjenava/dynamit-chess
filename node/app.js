/**
 * Dynamit Node Hackathon
 * Chess Game Server
 *
 * @author Anthony Bennett <anthony@dynamit.us>
 * @since 2013/04/27
 */

// include all the bits we need
var conf = require("./config.json"),
	connect = require("connect"),
	server = connect.createServer(
		connect.static(__dirname + "/../web/")
	).listen(conf.port),
	io = require("socket.io").listen(server, { log: conf.log }),
	game = require("./game");

// handler for when a user leaves the game
function leaveGameHandler(socket) {
	// leave game
	socket.get("data", function(err, data) {
		var endGame;

		// leave game room
		socket.leave(data.game._id);

		// remove player from game
		endGame = data.game.removePlayer(data.player);

		if (endGame) {
			socket.emit("game-ended", data.player);
		}
	});
}

// when a user connects to the server
io.sockets.on("connection", function(socket) {
	// send user a list of games
	socket.emit("list-games", game.listGames());

	// when a user joins a game
	socket.on("join-game", function(gameId) {
		// join the game with the given id
		var data = game.join(gameId);
		socket.set("data", data);

		// join the room for this game
		socket.join(data.game._id);

		// send user an updated list of games
		io.sockets.emit("list-games", game.listGames());

		// send the user their color (white, black, or null)
		socket.emit("set-color", data.player.color);

		// send the user the current state of the game
		socket.emit("updated", data.game.board.getSnapshot());
	});

	// when a user leaves a game
	socket.on("leave-game", function() {
		leaveGameHandler(socket);
	});

	// when the user moves a piece
	socket.on("moved", function(move) {
		// apply move and change the state of the board
		socket.get("data", function(err, data) {
			if (data.game.board.changeState(move)) {
				// send the user the current state of the game
				console.log(data.game.board.getSnapshot());
				io.sockets.in(data.game._id).emit(
					"updated", data.game.board.getSnapshot());
			} else {
				// notify the user the move was rejected
				socket.emit("rejected", move);
			}
		});
	});
});

// when a user disconnects from the server
io.sockets.on("disconnect", leaveGameHandler);