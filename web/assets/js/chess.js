(function (root, $) {
	root.Chess = (root.Chess || {});

	var socket = null;
	var player = null;

	var Init = function () {
		// connect to the server
		// leaving the argument blank allows it to auto detect where to connect
		socket = io.connect();

		socket.on('set-color', function(color) {
			player = color.toLowerCase();
		});

		socket.on('updated', UpdateBoard);

		$('div.square').droppable({
			accept: 'span.piece',
			drop: function(event, ui) {
				var origin = $(ui.draggable).parents('.square').attr('id');
				var destination = $(this).attr('id');
				var move = {origin: origin, destination: destination};
				socket.emit('moved', move);
			}
		});

		$('#join-game-btn').on('click', function (e) {
			e.preventDefault();

			var room = $('#join-game-name').val();

			socket.emit('join-game', room);

			$('#join-game').modal('hide');
		});

		socket.on('list-games', function (gamesList) {
			if (!gamesList || gamesList.length < 1)
				return;

			var $ul = $('#games-list');
			$ul.html("");
			_.each(gamesList, function (game) {
				var $link = $('<a>').attr('href', '#').text(game);

				$link.click(function (e) {
					e.preventDefault();

					socket.emit('join-game', game);
				});

				var $item = $('<li>');
				$item.html($link);
				$ul.append($item);
			});
		});
	};

	var UpdateBoard = function(snapshot) {
		$('div.square').each( function() {
			var state = _.findWhere(snapshot, { xy: $(this).attr('id') } );
			if(typeof state !== 'undefined'){

				var piece = GetPiece(state.type, state.color);

				if(piece.hasClass(player))
					piece.draggable({ revert: true });

				$(this).html(piece);

			} else {
				$(this).html('');
			}

		});
	};

	var GetPiece = function(type, color) {
		var piece = 0;
		switch(type) {
			case 'PAWN':
				piece = 9817;
				break;
			case 'ROOK':
				piece = 9814;
				break;
			case 'KNIGHT':
				piece = 9816;
				break;
			case 'BISHOP':
				piece = 9815;
				break;
			case 'QUEEN':
				piece = 9813;
				break;
			case 'KING':
				piece = 9812;
				break;
		}

		if(color != 'WHITE') {
			piece += 6;
		}
		var character = '&#' + piece + ';'

		return $('<span>')
			.addClass(color.toLowerCase())
			.addClass('piece')
			.addClass('grab')
			.html(character);
	};

	root.Chess.Init = Init;
}(this, jQuery));